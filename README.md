Getting started with Terraform 
==============================

We will deploy an EC2 (Ubuntu18.04) via terraform and we connect to the machine via SSH.

![](doc/_static/archi.png)

Prerequisites:
--------------

First, we need to have Terraform installed:
https://developer.hashicorp.com/terraform/install

If you are on windows, do the installation in WSL (so follow the LINUX installation)



Then we will need to create an IAM user (or to add rights to an existing one).

For that, go to the IAM service in AWS, in the **Users** section, click on *Create user*.

You can give the name you want to the user, then in the **Set permissions** part, click on **Attach policies directly** then search *AmazonEC2FullAccess* and 
select the policy.
You can then click *next* and *Create user*.

Now that your user is created, we will need to generate credentials to identify as this user.
Click on your user in IAM and go to the *Security credentials* tab.
Scroll down a bit and click on the *Create access key* button, select the *Command Line Interface (CLI)* use case, check the confirmation checkbox and click on *next*
then *Create access key*.
You know have your AWS Access Key ID and AWS Secret Access Key, i recommand you download the .csv file by clicking on the button on the bottom as it is
going to be the last time you can get the credentials.


We'll now need the AWS CLI to connect to AWS.
https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html

If you are on windows, do the installation in WSL (so follow the LINUX installation)

Now to allow authentification to aws api, you need to export your credentials.
you can use the aws cli:
````
aws configure 
````

And enter your access_key_id and your secret

set *eu-west-1* as the default region and *json* as the default format 


Step 1:
-------

If you don't have ssh keys on your computer generate one with :
````shell script
ssh-keygen 
````
Accept all default values for ssh-keygen (press ENTER multiple times)

Put the correct permission on your private key (the private key is the one that does NOT end with .pub):

````shell script
chmod 600 <PRIVATE_KEY>
````

Then edit your terraform.tfvars variables with ssh key/name:  

````hcl-terraform
aws_public_key_ssh_path = "<MY_PATH>/<MY_PUBLIC_KEY>.pub"
aws_private_key_ssh_path = "<MY_PATH>/<MY_PRIVATE_KEY>"
````


Step 2:
-------

```shell script
cd terraform # Go to terraform directory
terraform init # Get aws plugins
terraform plan # To get a dry run
terraform apply # Apply creation of the infra
```

Step 3:
-------

Test ssh connection, go to terraform.tfstate and get the value of the key : ssh_connect_cli.

This is the command line to connect to your machine:
 
````shell script
ssh -i ~/.ssh/id_rsa_aws ubuntu@ec2-xx-xx-xx-xx.eu-west-1.compute.amazonaws.com'
````

Example:

````json
{
  "version": 4,
  "terraform_version": "0.12.9",
  "serial": 5,
  "outputs": {
    "ssh_connect_cli": {
      "value": "ssh -i ~/.ssh/id_rsa_aws ubuntu@ec2-xx-xx-xx-xx.eu-west-1.compute.amazonaws.com",
      "type": "string"
    }
  }
}
````


Step 4:
-------

Now that we tested that everything works properly, we can destroy our infra.

To do so:

````shell script
terraform destroy
````